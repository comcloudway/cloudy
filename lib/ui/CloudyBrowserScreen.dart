import 'dart:convert';

import 'package:Cloudy/colors/GruvboxColors.dart';
import 'package:Cloudy/colors/config.dart';
import 'package:Cloudy/ui/modules/Cloudy.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;

class CloudyBrowserScreen extends StatefulWidget {
  _CLoudyBrowserScreenState createState() => _CLoudyBrowserScreenState();
}

class _CLoudyBrowserScreenState extends State<CloudyBrowserScreen> {
  TextEditingController textEditingController;
  String term = "cloudy:home";
  Widget _view = Cloudy('home');
  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController(text: term);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Feather.home),
            onPressed: () {
              //go to main page "cloudy:home"
              goTo('cloudy:home');
            }),
        actions: <Widget>[
          IconButton(
              icon: Icon(Feather.send),
              onPressed: () {
                //Open Page
                search();
              }),
          IconButton(
              icon: Icon(Feather.more_vertical),
              onPressed: () {
                //Options
                showMenu(
                    context: context,
                    position: RelativeRect.fromLTRB(
                        MediaQuery.of(context).size.width,
                        45,
                        0,
                        MediaQuery.of(context).size.height - 45),
                    items: [
                      PopupMenuItem(
                          child: Row(
                        children: <Widget>[
                          Icon(Feather.share),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                          ),
                          Text("Share")
                        ],
                      )),
                      PopupMenuItem(
                          child: Row(
                        children: <Widget>[
                          Icon(Feather.server),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                          ),
                          Text("Domainlist")
                        ],
                      )),
                      PopupMenuItem(
                          child: Row(
                        children: <Widget>[
                          Icon(Feather.settings),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                          ),
                          Text("Settings")
                        ],
                      )),
                    ]);
              })
        ],
        title: Padding(
          child: Container(
            child: TextField(
              onChanged: (value) {
                setState(() {
                  term = value;
                });
              },
              controller: textEditingController,
              cursorColor: defaultColorScheme.primaryVariant,
              maxLines: 1,
              style: TextStyle(color: GruvboxColors.light1),
              decoration: InputDecoration.collapsed(hintText: "URL"),
            ),
            padding: EdgeInsets.only(left: 5, right: 2.5, top: 0, bottom: 0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: GruvboxColors.dark1),
          ),
          padding: EdgeInsets.all(5),
        ),
      ),
      body: _view,
    );
  }

  void search() async {
    if (term.contains(':')) {
      //try goTo
      goTo(term);
    } else {
      goTo('public:' + term);
      setState(() {
        term = 'public:' + term;
      });
    }
  }

  void goTo(String url) async {
    List<String> p = url.split(':');
    setState(() {
      term = url;
    });
    switch (p[0]) {
      case "cloudy":
        // cloudy build in functions
        setState(() {
          _view = Cloudy(p[1]);
        });
        break;
      case "cloudy-dev":
        // beta functions
        break;
      case "public":
        //read github database to resolve url
        var resp = await http
            .get('https://gitlab.com/comcloudway/cloudy/-/raw/db/db.json');
        if (resp.statusCode >= 200 && resp.statusCode < 300) {
          //success
          var data = json.decode(resp.body);
          String domain = "";
          bool suc = true;
          try {
            domain = data["domains"][p[1]];
          } catch (e) {
            suc = false;
            setState(() {
              _view = Cloudy('page-not-found');
            });
          }
          if (suc) {
            setState(() {
              _view = WebviewScaffold(
                url: domain,
              );
            });
          }
        } else {
          setState(() {
            _view = Cloudy('page-not-found');
          });
        }
        break;
      default:
        //show error message -> navigate to cloudy:page-not-found
        setState(() {
          _view = Cloudy('page-not-found');
        });
    }
  }
}
