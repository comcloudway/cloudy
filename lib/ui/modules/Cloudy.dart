import 'dart:math';

import 'package:flutter/cupertino.dart';

class Cloudy extends StatefulWidget {
  _CloudyState createState() => _CloudyState();
  String location = " home";
  Cloudy(this.location);
}

class _CloudyState extends State<Cloudy> {
  @override
  Widget build(BuildContext context) {
    switch (widget.location) {
      case "home":
        return SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  child: Image.asset(
                    'assets/icon.png',
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  padding: EdgeInsets.all(30),
                ),
                Text(
                  "Cloudy",
                  style: TextStyle(fontSize: 30),
                )
              ],
            ),
          ),
        );
      case "page-not-found":
      default:
        return SingleChildScrollView(
            child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                child: Transform(
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/icon.png',
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                  transform: Matrix4.rotationX(pi),
                ),
                padding: EdgeInsets.all(30),
              ),
              Text(
                "There are to many clouds in the Way",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30),
              ),
              Text("Cloudn't find any results")
            ],
          ),
        ));
    }
  }
}
