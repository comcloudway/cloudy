import 'package:Cloudy/colors/colorSchemes.dart';
import 'package:Cloudy/colors/themes.dart';
import 'package:flutter/material.dart';

ColorScheme defaultColorScheme = gruvboxDarkScheme;

ThemeData defaultTheme = gruvboxDarkTheme;
