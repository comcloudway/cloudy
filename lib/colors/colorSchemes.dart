import 'package:Cloudy/colors/GruvboxColors.dart';
import 'package:flutter/material.dart';

ColorScheme gruvboxDarkScheme = ColorScheme(
    primary: GruvboxColors.neutralBlue,
    primaryVariant: GruvboxColors.fadedBlue,
    secondary: GruvboxColors.neutralGreen,
    secondaryVariant: GruvboxColors.fadedGreen,
    surface: GruvboxColors.light0,
    background: GruvboxColors.dark0,
    error: GruvboxColors.fadedRed,
    onPrimary: GruvboxColors.light0,
    onSecondary: GruvboxColors.light0,
    onSurface: GruvboxColors.light0,
    onBackground: GruvboxColors.light0,
    onError: GruvboxColors.light0,
    brightness: Brightness.dark);
