import 'package:Cloudy/colors/config.dart';
import 'package:flutter/material.dart';

ThemeData gruvboxDarkTheme = ThemeData(
  brightness: defaultColorScheme.brightness,
  colorScheme: defaultColorScheme,
  primaryColor: defaultColorScheme.background,
  backgroundColor: defaultColorScheme.background,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);
