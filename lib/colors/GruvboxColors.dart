import 'package:flutter/material.dart';

class GruvboxColors {
  //static Color  = Color(0x);

  //DARK
  static Color dark0Hard = Color(0xFF2d2021);
  static Color dark0 = Color(0xFF282828);
  static Color dark0Soft = Color(0xFF32302f);
  static Color dark1 = Color(0xFF3c3836);
  static Color dark2 = Color(0xFF504945);
  static Color dark3 = Color(0xFF665c54);
  static Color dark4 = Color(0xFF7c6f64);
  static Color dark4256 = Color(0xFF7c6f64);

  //GRAY
  static Color gray245 = Color(0xFF928374);
  static Color gray244 = Color(0xFF928374);

  //LIGHT
  static Color light0Hard = Color(0xFFf9f5d7);
  static Color light0 = Color(0xFFfbf1c7);
  static Color light0Soft = Color(0xFFf2e5bc);
  static Color light1 = Color(0xFFebdbb2);
  static Color light2 = Color(0xFFd5c4a1);
  static Color light3 = Color(0xFFbdae93);
  static Color light4 = Color(0xFFa89984);
  static Color light4256 = Color(0xFFa89984);

  //BRIGHT
  static Color brightRed = Color(0xFFfb4934);
  static Color brightGreen = Color(0xFFb8bb26);
  static Color brightYellow = Color(0xFFfabd2f);
  static Color brightBlue = Color(0xFF83a598);
  static Color brightPurple = Color(0xFFd3869b);
  static Color brightAqua = Color(0xFF8ec07c);
  static Color brightOrange = Color(0xFFfe8019);

  //NEUTRAL
  static Color neutralRed = Color(0xFFcc241d);
  static Color neutralGreen = Color(0xFF98971a);
  static Color neutralYellow = Color(0xFFd79921);
  static Color neutralBlue = Color(0xFF458588);
  static Color neutralPurple = Color(0xFFb16286);
  static Color neutralAqua = Color(0xFF689d6a);
  static Color neutralOrange = Color(0xFFd65d0e);

  //FADED
  static Color fadedRed = Color(0xFF9d0006);
  static Color fadedGreen = Color(0xFF79740e);
  static Color fadedYellow = Color(0xFFb57614);
  static Color fadedBlue = Color(0xFF076678);
  static Color fadedPurple = Color(0xFF8f3f71);
  static Color fadedAqua = Color(0xFF427b58);
  static Color fadedOrange = Color(0xFFaf3a03);
}
