import 'package:Cloudy/colors/GruvboxColors.dart';
import 'package:Cloudy/colors/config.dart';
import 'package:Cloudy/ui/CloudyBrowserScreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(CloudyApp());
}

class CloudyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: defaultTheme,
      home: CloudyBrowserScreen(),
    );
  }
}
